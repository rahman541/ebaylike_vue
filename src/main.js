import Vue from 'vue'
import App from './App.vue'
import Router from './routes.js'
import VeeValidate from 'vee-validate'

import VueResource from 'vue-resource'

import Auth from './packages/auth/Auth'

Vue.use(VueResource)
Vue.use(Auth)
Vue.use(VeeValidate)

Vue.http.options.root = 'http://127.0.0.1:8000'
Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken()
Vue.http.interceptors.push((request, next) => {
  next(res => {
    if (res.status == 404)
      swal(res.status.toString(), res.body.message, 'error')
  })
})

Router.beforeEach(
  (to, from, next) => {
    if (to.matched.some(record => record.meta.forVisitors)) {
      if (Vue.auth.isAuthenticated()) {
        next({
          path: '/feed'
        })
      } else next()
    }
    else if (to.matched.some(record => record.meta.forAuth)) {
      if (! Vue.auth.isAuthenticated()) {
        next({
          path: '/login'
        })
      } else next()
    }
    else next()
  }
)

new Vue({
  el: '#app',
  render: h => h(App),
  router: Router,
})
